import React from 'react';
import { Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import DashboardScreen from './src/screens/DashboardScreen';
import IncomeScreen from './src/screens/IncomeScreen';
import ExpensesScreen from './src/screens/ExpensesScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Dashboard"
      >
        <Tab.Screen
          name="IncomeScreen"
          component={IncomeScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <>
                <MaterialIcons
                  name="home"
                  color={focused ? 'black' : '#ccc'}
                  size={20}
                />

              </>
            ),
            tabBarLabel: ({ focused }) => {
              return <Text style={{ color: focused ? 'black' : "#ccc", fontSize: 10, fontWeight: "bold", bottom: 3 }}>Income</Text>;
            },
          }}
        />
        <Tab.Screen
          name="Dashboard"
          component={DashboardScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <>
                <MaterialIcons
                  name="dashboard"
                  color={focused ? 'black' : '#ccc'}
                  size={20}
                />

              </>
            ),
            tabBarLabel: ({ focused }) => {
              return <Text style={{ color: focused ? 'black' : "#ccc", fontSize: 10, fontWeight: "bold", bottom: 3 }}>Dashboard</Text>;
            },
          }}
        />
        <Tab.Screen
          name="ExpensesScreen"
          component={ExpensesScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <>
                <MaterialIcons
                  name="home"
                  color={focused ? 'black' : '#ccc'}
                  size={20}
                />

              </>
            ),
            tabBarLabel: ({ focused }) => {
              return <Text style={{ color: focused ? 'black' : "#ccc", fontSize: 10, fontWeight: "bold", bottom: 3 }}>Expenses</Text>;
            },
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;