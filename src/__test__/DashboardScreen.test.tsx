import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import axios from 'axios'; // Import Axios
import DashboardScreen from '../screens/DashboardScreen';

jest.mock('axios');

describe('DashboardScreen Component', () => {
  it('renders loading indicator while fetching data', async () => {

    axios.get.mockResolvedValueOnce({ data: [] }).mockResolvedValueOnce({ data: [] });

    const { getByTestId } = render(<DashboardScreen />);
    const loadingIndicator = getByTestId('loading-indicator');

    expect(loadingIndicator).toBeDefined();

    await waitFor(() => expect(loadingIndicator).not.toBeInTheDocument());
  });

  it('renders income, expenses, and total balance after data is fetched', async () => {
    const mockIncomeData = [{ id: '1', amount: 100 }];
    const mockExpenseData = [{ id: '1', amount: 50 }];

    axios.get.mockResolvedValueOnce({ data: mockIncomeData }).mockResolvedValueOnce({ data: mockExpenseData });

    const { getByText } = render(<DashboardScreen />);

    await waitFor(() => {
      expect(getByText('Total Income:')).toBeDefined();
      expect(getByText('Total Expenses:')).toBeDefined();
      expect(getByText('Total Balance:')).toBeDefined();
      expect(getByText('$100')).toBeDefined(); // Assuming formatRupiahAngka is tested elsewhere
      expect(getByText('$50')).toBeDefined();
      expect(getByText('$50')).toBeDefined();
    });
  });

  it('displays error message when fetching data fails', async () => {
    axios.get.mockRejectedValueOnce(new Error('Failed to fetch data'));

    const { getByText } = render(<DashboardScreen />);

    await waitFor(() => {
      expect(getByText('Failed to load income data')).toBeDefined();
      expect(getByText('Failed to load expenses data')).toBeDefined();
    });
  });
});
