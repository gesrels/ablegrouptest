import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import axios from 'axios'; 
import ExpensesList from '../screens/ExpensesScreen';

jest.mock('axios');

describe('ExpensesList Component', () => {
  it('renders loading indicator when fetching data', async () => {
    axios.get.mockResolvedValue({ data: [] });

    const { getByTestId } = render(<ExpensesList />);
    const loadingIndicator = getByTestId('loading-indicator');

    expect(loadingIndicator).toBeDefined();

    await waitFor(() => expect(loadingIndicator).not.toBeInTheDocument());
  });

  it('renders list of expense transactions after data is fetched', async () => {
    const mockData = [
      { id: '1', desc: 'Transaction 1', amount: 100 },
      { id: '2', desc: 'Transaction 2', amount: 200 }
    ];

    axios.get.mockResolvedValue({ data: mockData });

    const { getByText } = render(<ExpensesList />);

    await waitFor(() => {
      expect(getByText('Transaction 1')).toBeDefined();
      expect(getByText('Transaction 2')).toBeDefined();
    });
  });

  it('displays error message when fetching data fails', async () => {
    axios.get.mockRejectedValue(new Error('Failed to fetch data'));

    const { getByText } = render(<ExpensesList />);

    await waitFor(() => {
      expect(getByText('Failed to load expenses data')).toBeDefined();
    });
  });

  it('calls fetchData function when refresh control is triggered', async () => {
    const fetchDataMock = jest.fn();

    axios.get.mockResolvedValue({ data: [] });

    const { getByTestId } = render(<ExpensesList fetchData={fetchDataMock} />);
    const refreshControl = getByTestId('refresh-control');

    fireEvent(refreshControl, 'refresh');

    await waitFor(() => expect(fetchDataMock).toHaveBeenCalledTimes(1));
  });
});
