import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import axios from 'axios';
import IncomeList from '../screens/IncomeScreen';

jest.mock('axios');

describe('IncomeList Component', () => {
  it('renders loading indicator when fetching data', async () => {
    axios.get.mockResolvedValue({ data: [] });

    const { getByTestId } = render(<IncomeList />);
    const loadingIndicator = getByTestId('loading-indicator');

    expect(loadingIndicator).toBeDefined();

    await waitFor(() => expect(loadingIndicator).not.toBeInTheDocument());
  });

  it('renders list of income transactions after data is fetched', async () => {
    const mockData = [
      { id: '1', desc: 'Transaction 1', amount: 100 },
      { id: '2', desc: 'Transaction 2', amount: 200 }
    ];

    // Mock Axios get method to return a promise that resolves with mock data
    axios.get.mockResolvedValue({ data: mockData });

    const { getByText } = render(<IncomeList />);

    await waitFor(() => {
      expect(getByText('Transaction 1')).toBeDefined();
      expect(getByText('Transaction 2')).toBeDefined();
    });
  });

  it('displays error message when fetching data fails', async () => {
    axios.get.mockRejectedValue(new Error('Failed to fetch data'));

    const { getByText } = render(<IncomeList />);

    await waitFor(() => {
      expect(getByText('Failed to load income data')).toBeDefined();
    });
  });

  it('calls fetchData function when refresh control is triggered', async () => {
    const fetchDataMock = jest.fn();

    axios.get.mockResolvedValue({ data: [] });

    const { getByTestId } = render(<IncomeList fetchData={fetchDataMock} />);
    const refreshControl = getByTestId('refresh-control');

    fireEvent(refreshControl, 'refresh');

    await waitFor(() => expect(fetchDataMock).toHaveBeenCalledTimes(1));
  });
});
