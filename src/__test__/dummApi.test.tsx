// src/api/dummyApi.test.ts
import { fetchTransactions } from '../api/dummyApi';

describe('fetchTransactions', () => {
  test('fetches income transactions', async () => {
    const transactions = await fetchTransactions('income');
    expect(transactions).toHaveLength(2); // Adjust this based on your dummy data
    expect(transactions[0].type).toEqual('income');
  });

  test('fetches expense transactions', async () => {
    const transactions = await fetchTransactions('expense');
    expect(transactions).toHaveLength(2); // Adjust this based on your dummy data
    expect(transactions[0].type).toEqual('expense');
  });
});