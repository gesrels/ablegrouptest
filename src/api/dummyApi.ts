// dummyApi.ts
export interface Transaction {
    id: string;
    type: 'income' | 'expense';
    amount: number;
    date: string;
    desc : string;
  }
  
  const incomeData: Transaction[] = [
    { id: '1', type: 'income', amount: 1000, date: '2024-04-01', desc :'Salary'},
    { id: '2', type: 'income', amount: 1500, date: '2024-04-02', desc :'Business' },
    { id: '3', type: 'income', amount: 2390, date: '2024-04-01', desc :'Sales'},
    { id: '4', type: 'income', amount: 9300, date: '2024-04-02', desc :'Charity' },
    { id: '5', type: 'income', amount: 4890, date: '2024-04-01', desc :'Business'},
    { id: '6', type: 'income', amount: 4320, date: '2024-04-02', desc :'Salary' },
    { id: '7', type: 'income', amount: 6540, date: '2024-04-01', desc :'Sales'},
    { id: '8', type: 'income', amount: 3940, date: '2024-04-02', desc :'Salary' },
  ];
  
  const expenseData: Transaction[] = [
    { id: '1', type: 'expense', amount: 5000, date: '2024-04-01', desc :'Rent' },
    { id: '2', type: 'expense', amount: 1000, date: '2024-04-02', desc :'Consumtive' },
    { id: '3', type: 'expense', amount: 1200, date: '2024-04-01', desc :'Foods' },
    { id: '4', type: 'expense', amount: 4920, date: '2024-04-02', desc :'Shopping' },
    { id: '8', type: 'expense', amount: 3100, date: '2024-04-01', desc :'Foods' },
    { id: '5', type: 'expense', amount: 9200, date: '2024-04-02', desc :'Trip' },
    { id: '6', type: 'expense', amount: 5000, date: '2024-04-01', desc :'Rent' },
    { id: '7', type: 'expense', amount: 9800, date: '2024-04-02', desc :'Hiking' },
  ];
  
  export const fetchTransactions = async (type: 'income' | 'expense'): Promise<Transaction[]> => {
    return type === 'income' ? incomeData : expenseData;
  };