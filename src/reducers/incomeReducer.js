const initialState = {
    incomeTransactions: [],
    isLoading: false,
  };
  
  const incomeReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_INCOME_TRANSACTIONS_REQUEST':
        return {
          ...state,
          isLoading: true,
        };
      case 'FETCH_INCOME_TRANSACTIONS_SUCCESS':
        return {
          ...state,
          isLoading: false,
          incomeTransactions: action.payload,
        };
      case 'FETCH_INCOME_TRANSACTIONS_FAILURE':
        return {
          ...state,
          isLoading: false,
        };
      default:
        return state;
    }
  };
  
  export default incomeReducer;