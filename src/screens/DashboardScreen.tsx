import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import axios from 'axios'; // Import Axios
import common from '../utils/common';

const DashboardScreen: React.FC = () => {
  const [income, setIncome] = useState<number>(0);
  const [expenses, setExpenses] = useState<number>(0);
  const [isLoading, setIsLoading] = useState<boolean>(true); // Add loading state

  useEffect(() => {
    const fetchSummary = async () => {
      try {
        const incomeResponse = await axios.get('https://grimax.id/incomeFaker.php');
        const res = incomeResponse;
        const expenseResponse = await axios.get('https://grimax.id/expensesFaker.php'); 
        const res2 = expenseResponse;

        sumAmountsIncome(res.data);
        sumAmountsExpenses(res2.data);
        setIsLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setIsLoading(false);
      }
    };

    fetchSummary();
  }, []);

  const sumAmountsExpenses = (expenses:any) => {
    let total = 0;
    for (const expense of expenses) {
      total += expense.amount;
    }    
    setExpenses(total);
  };

  const sumAmountsIncome = (incomes:any) => {
    let total = 0;
    for (const income of incomes) {
      total += income.amount;
    }    
    setIncome(total);
  };

  const totalBalance = income - expenses;

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Your Income and Expenses</Text>
      <View style={styles.summaryContainer}>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryLabel}>Total Income:</Text>
          <Text style={styles.summaryValue}>${common.formatRupiahAngka(income)}</Text>
        </View>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryLabel}>Total Expenses:</Text>
          <Text style={styles.summaryValue}>${common.formatRupiahAngka(expenses)}</Text>
        </View>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryLabel}>Total Balance:</Text>
          <Text style={styles.summaryValue}>${common.formatRupiahAngka(totalBalance)}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 70
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: "black"
  },
  summaryContainer: {
    width: '80%',
  },
  summaryItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
    color: "black"
  },
  summaryLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "black"
  },
  summaryValue: {
    fontSize: 18,
    color: "black"
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DashboardScreen;
