import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, StyleSheet, 
  ActivityIndicator, RefreshControl, Animated, Alert } from 'react-native';
import axios from 'axios'; // Import Axios

import common from '../utils/common';

const IncomeList = () => {
  const [incomeTransactions, setIncomeTransactions] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const bounceAnim = useState(new Animated.Value(0.5))[0];

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsLoading(true);
    setIsRefreshing(true);
    try {
      const response = await axios.get('https://grimax.id/incomeFaker.php'); // Replace with your Faker API URL
      const res = response;
      setIncomeTransactions(res.data);
      setIsRefreshing(false);
      setIsLoading(false);
      bounceAnimation();
    } catch (error) {
      console.error('Error fetching data:', error);
      setIsRefreshing(false);
      setIsLoading(false);
      Alert.alert('Error', 'Failed to load income data');
    }
  };

  const bounceAnimation = () => {
    Animated.spring(
      bounceAnim,
      {
        toValue: 1,
        friction: 4,
        tension: 40,
        useNativeDriver: true
      }
    ).start();
  }

  const renderItem = ({ item }) => (
    <Animated.View style={{ alignSelf: "center", transform: [{ scale: bounceAnim }] }}>
      <View style={styles.item}>
        <View style={{ width: '80%' }}>
          <Text style={styles.desc}>{item.desc}</Text>
          <Text style={styles.text}>{common.dateFormat(item.date)}</Text>
        </View>
        <View style={{ width: '20%' }}>
          <Text style={styles.amount}>${common.formatRupiahAngka(item.amount)}</Text>
        </View>
      </View>
    </Animated.View>
  );

  const handleRefresh = () => {
    fetchData();
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <FlatList
      data={incomeTransactions}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      ListEmptyComponent={<Text>No income transactions</Text>}
      refreshControl={
        <RefreshControl
          refreshing={isRefreshing}
          onRefresh={handleRefresh}
          colors={['#0000ff']}
        />
      }
    />
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: "row",
    padding: 15,
    borderRadius: 15,
    backgroundColor: "#fff",
    width: '90%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0.5,
    },
    shadowOpacity: 0,
    shadowRadius: 0.5,
    elevation: 2,
    borderWidth: 0.5,
    borderColor: "#ccc",
    margin: 5,
    marginTop: 10
  },
  text: {
    color: "#000"
  },
  desc: {
    color: "#000",
    fontSize: 18,
    marginBottom: 5
  },
  amount: {
    color: "#000",
    fontSize: 25,
    marginTop: 8
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default IncomeList;