const common = {
    dateFormat(date: string | number | Date): string {
        let dates = new Date(date);
        var days: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
        var months: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des'];
        var d = new Date(dates);
        let format = days[d.getDay()] + ', ' + dates.getDate() + ' ' + months[d.getMonth()] + ' ' + dates.getFullYear();
        return format;
    },
    formatRupiahAngka(angka : string) {
        let ang = angka.toString();
        let angkar = ang.split('.');
        let angkas = angkar[0];
        var number_string = angkas.replace(/[^,\d]/g, '').toString(),
          split = number_string.split(','),
          sisa = split[0].length % 3,
          rupiah = split[0].substr(0, sisa),
          ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    
        let separator = '';
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? '' + rupiah + ',' + split[1] : rupiah;
        return rupiah;
      },
};

export default common;